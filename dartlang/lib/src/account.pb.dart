///
//  Generated code. Do not modify.
//  source: account.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' show int, bool, double, String, List, override;

import 'package:protobuf/protobuf.dart' as $pb;

class SignUpRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('SignUpRequest', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'email')
    ..aOS(2, 'password')
    ..hasRequiredFields = false
  ;

  SignUpRequest() : super();
  SignUpRequest.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SignUpRequest.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SignUpRequest clone() => new SignUpRequest()..mergeFromMessage(this);
  SignUpRequest copyWith(void Function(SignUpRequest) updates) => super.copyWith((message) => updates(message as SignUpRequest));
  $pb.BuilderInfo get info_ => _i;
  static SignUpRequest create() => new SignUpRequest();
  static $pb.PbList<SignUpRequest> createRepeated() => new $pb.PbList<SignUpRequest>();
  static SignUpRequest getDefault() => _defaultInstance ??= create()..freeze();
  static SignUpRequest _defaultInstance;
  static void $checkItem(SignUpRequest v) {
    if (v is! SignUpRequest) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get email => $_getS(0, '');
  set email(String v) { $_setString(0, v); }
  bool hasEmail() => $_has(0);
  void clearEmail() => clearField(1);

  String get password => $_getS(1, '');
  set password(String v) { $_setString(1, v); }
  bool hasPassword() => $_has(1);
  void clearPassword() => clearField(2);
}

class SignInRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('SignInRequest', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'email')
    ..aOS(2, 'password')
    ..hasRequiredFields = false
  ;

  SignInRequest() : super();
  SignInRequest.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SignInRequest.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SignInRequest clone() => new SignInRequest()..mergeFromMessage(this);
  SignInRequest copyWith(void Function(SignInRequest) updates) => super.copyWith((message) => updates(message as SignInRequest));
  $pb.BuilderInfo get info_ => _i;
  static SignInRequest create() => new SignInRequest();
  static $pb.PbList<SignInRequest> createRepeated() => new $pb.PbList<SignInRequest>();
  static SignInRequest getDefault() => _defaultInstance ??= create()..freeze();
  static SignInRequest _defaultInstance;
  static void $checkItem(SignInRequest v) {
    if (v is! SignInRequest) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get email => $_getS(0, '');
  set email(String v) { $_setString(0, v); }
  bool hasEmail() => $_has(0);
  void clearEmail() => clearField(1);

  String get password => $_getS(1, '');
  set password(String v) { $_setString(1, v); }
  bool hasPassword() => $_has(1);
  void clearPassword() => clearField(2);
}

class SignInResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('SignInResponse', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'token')
    ..hasRequiredFields = false
  ;

  SignInResponse() : super();
  SignInResponse.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SignInResponse.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SignInResponse clone() => new SignInResponse()..mergeFromMessage(this);
  SignInResponse copyWith(void Function(SignInResponse) updates) => super.copyWith((message) => updates(message as SignInResponse));
  $pb.BuilderInfo get info_ => _i;
  static SignInResponse create() => new SignInResponse();
  static $pb.PbList<SignInResponse> createRepeated() => new $pb.PbList<SignInResponse>();
  static SignInResponse getDefault() => _defaultInstance ??= create()..freeze();
  static SignInResponse _defaultInstance;
  static void $checkItem(SignInResponse v) {
    if (v is! SignInResponse) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get token => $_getS(0, '');
  set token(String v) { $_setString(0, v); }
  bool hasToken() => $_has(0);
  void clearToken() => clearField(1);
}

class Account extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Account', package: const $pb.PackageName('boringtodoapp'))
    ..aOS(1, 'id')
    ..aOS(2, 'email')
    ..aOS(3, 'password')
    ..aOS(4, 'token')
    ..hasRequiredFields = false
  ;

  Account() : super();
  Account.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Account.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Account clone() => new Account()..mergeFromMessage(this);
  Account copyWith(void Function(Account) updates) => super.copyWith((message) => updates(message as Account));
  $pb.BuilderInfo get info_ => _i;
  static Account create() => new Account();
  static $pb.PbList<Account> createRepeated() => new $pb.PbList<Account>();
  static Account getDefault() => _defaultInstance ??= create()..freeze();
  static Account _defaultInstance;
  static void $checkItem(Account v) {
    if (v is! Account) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get id => $_getS(0, '');
  set id(String v) { $_setString(0, v); }
  bool hasId() => $_has(0);
  void clearId() => clearField(1);

  String get email => $_getS(1, '');
  set email(String v) { $_setString(1, v); }
  bool hasEmail() => $_has(1);
  void clearEmail() => clearField(2);

  String get password => $_getS(2, '');
  set password(String v) { $_setString(2, v); }
  bool hasPassword() => $_has(2);
  void clearPassword() => clearField(3);

  String get token => $_getS(3, '');
  set token(String v) { $_setString(3, v); }
  bool hasToken() => $_has(3);
  void clearToken() => clearField(4);
}

